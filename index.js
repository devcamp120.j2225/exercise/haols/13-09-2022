// import thư viện express
const express = require("express");
// Import thư viện path
const path = require("path");
// Import thư viện mongoose
const mongoose = require("mongoose");

// Kết nối với MongoDB
mongoose.connect("mongodb://localhost:27017/CRUD_LucyDice", function (error) {
  if (error) throw error;
  console.log("Successfully connected");
});

// import middleware
const { dateTimeMiddleware } = require("./app/middlewares/dateTimeMiddleware");
const {
  reqMethodMiddleware,
} = require("./app/middlewares/reqMethodMiddleware");

// import routes
const { randomDiceRouter } = require("./app/routes/randomDiceRouter");
const { userRouter } = require("./app/routes/userRouter");
const { diceHistoryRouter } = require("./app/routes/diceHistoryRouter");
const { prizeRouter } = require("./app/routes/prizeRouter");
const { voucherRouter } = require("./app/routes/voucherRouter");

// import model
// const priceModel = require("./app/models/prizeModel");
// const voucherModel = require("./app/models/voucherModel");
// ..................................................
// khai báo ứng dụng app
const app = express();

// Sử dụng đc body json
app.use(express.json());

const port = 8000;
// Use middleware
app.use(dateTimeMiddleware);
app.use(reqMethodMiddleware);

// Use routes
app.use(randomDiceRouter);
app.use(userRouter);
app.use(diceHistoryRouter);
app.use(prizeRouter);
app.use(voucherRouter);
// Sử dụng đc body json
app.use(express.json());

// Sử dụng đc unicode
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Thêm middleware static vào để hiển thị ảnh
app.use(express.static(__dirname + "/views"));

// Tạo API trả ra file giao diện
app.get("/", (req, res) => {
  console.log(__dirname);
  res.sendFile(path.join(__dirname + "/views/Ma nguon Lucky Dice Casino.html"));
});

// Thêm middleware static vào để hiển thị ảnh
app.use(express.static(__dirname + "/views"));
app.listen(port, () => {
  console.log("App listening on port: ", port);
});
