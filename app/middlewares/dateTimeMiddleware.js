const dateTimeMiddleware = (req, res, next) => {
  let today = new Date();
  console.log(`Current: ${today}`);
  next();
};
module.exports = { dateTimeMiddleware };
