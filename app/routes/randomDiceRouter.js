// Import thư viện express
const express = require("express");

// Tạo router
const randomDiceRouter = express.Router();

// Tạo API
randomDiceRouter.get("/random-number", (req, res) => {
  let randomNum = Math.floor(Math.random() * 6 + 1);
  console.log(`Throw: ${randomNum}`);
  res.status(200).json({
    throw: randomNum,
  });
});
module.exports = { randomDiceRouter };
