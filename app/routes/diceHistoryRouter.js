// Import thư viện express
const express = require("express");

// Tạo router
const diceHistoryRouter = express.Router();

//Import controllers
const {
  createDiceHistory,
  getAllDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById,
} = require("../controllers/diceHistoryController");

//Create
diceHistoryRouter.post("/dice-histories", createDiceHistory);
//Get all
diceHistoryRouter.get("/dice-histories", getAllDiceHistory);
//Get by ID
diceHistoryRouter.get("/dice-histories/:diceHistoryId", getDiceHistoryById);
//Update by ID
diceHistoryRouter.put("/dice-histories/:diceHistoryId", updateDiceHistoryById);
//Delete by ID
diceHistoryRouter.delete(
  "/dice-histories/:diceHistoryId",
  deleteDiceHistoryById
);
module.exports = { diceHistoryRouter };
