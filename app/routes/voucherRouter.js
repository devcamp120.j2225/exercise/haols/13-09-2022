// Import thư viện express
const express = require("express");

// Tạo router
const voucherRouter = express.Router();

// Import controllers
const { createVoucher,
    getAllVoucher,
    getVoucherById,
    uppdateVoucherById,
    deleteVoucherById,} = require("../controllers/voucherController");

// Create an user
voucherRouter.post("/vouchers", createVoucher);
// Get all vouchers
voucherRouter.get("/vouchers", getAllVoucher);
// Get user by id
voucherRouter.get("/vouchers/:voucherId", getVoucherById);
//Update user by id
voucherRouter.put("/vouchers/:voucherId", uppdateVoucherById);
//Delete user by id
voucherRouter.delete("/vouchers/:voucherId", deleteVoucherById);
module.exports = { voucherRouter };