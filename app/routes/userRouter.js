// Import thư viện express
const express = require("express");

// Tạo router
const userRouter = express.Router();

// Import controllers
const { createUser, getAllUsers, getUserById,uppdateUserById,deleteUserById} = require("../controllers/userController");

// Create an user
userRouter.post("/users", createUser);
// Get all users
userRouter.get("/users", getAllUsers);
// Get user by id
userRouter.get("/users/:userId", getUserById);
//Update user by id
userRouter.put("/users/:userId", uppdateUserById);
//Delete user by id
userRouter.delete("/users/:userId", deleteUserById);
module.exports = { userRouter };
