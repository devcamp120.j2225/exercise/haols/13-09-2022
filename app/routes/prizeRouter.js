// Import thư viện express
const express = require("express");

// Tạo router
const prizeRouter = express.Router();

// Import controllers
const { createPrize,
    getAllPrize,
    getPrizeById,
    uppdatePrizeById,
    deletePrizeById,} = require("../controllers/prizeController");

// Create an user
prizeRouter.post("/prizes", createPrize);
// Get all users
prizeRouter.get("/prizes", getAllPrize);
// Get user by id
prizeRouter.get("/prizes/:prizeId", getPrizeById);
//Update user by id
prizeRouter.put("/prizes/:prizeId", uppdatePrizeById);
//Delete user by id
prizeRouter.delete("/prizes/:prizeId", deletePrizeById);
module.exports = { prizeRouter };