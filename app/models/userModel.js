// Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo user model
const userModel = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    },
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("user", userModel);
