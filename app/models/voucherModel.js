// Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo user model
const voucherModel = new Schema(
  {
    code: {
      type: String,
      unique: true,
      required: true,
    },
    discount: {
      type: String,
      required: true,
    },
    note: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("voucher", voucherModel);
