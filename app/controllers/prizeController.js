// Import thư viện mongoose
const mongoose = require("mongoose");

// Import user model
const prizeModel = require("../models/prizeModel");

// Create
const createPrize = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;
    console.log(body);
    // B2: Validate dữ liệu
    if (!body.name) {
      res.status(400).json({
        message: "Name is required!",
      });
    }
    
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newPrize = {
      _id: mongoose.Types.ObjectId(),
      name: body.name,
      description: body.description
    };
    prizeModel.create(newPrize, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(201).json({
        message: "Create a new prize successfully",
        newPrize: data,
      });
    });
  };

  // Get all 
const getAllPrize = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.find((error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Get all prize successfully",
        users: data,
      });
    });
  };

  // Get by Id
const getPrizeById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let prizeId = req.params.prizeId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "Prize ID is invalid!",
      });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.findById(prizeId, (error, data) => {
      if (error) {
        res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Get user by ID successfully!",
        prize: data,
      });
    });
  };

  // Update user by Id
const uppdatePrizeById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let prizeId = req.params.prizeId;
    let body = req.body;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "User ID is invalid!",
      });
    }
    if (body.name !== undefined && body.name == "") {
      res.status(400).json({
        message: "Name is required!",
      });
    }
    
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let updatePrize = {
        name: body.name,
        description: body.description
    };
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(201).json({
        message: "Update prize successfully",
        updatePrize: data,
      });
    });
  };
  // Delete user by id
const deletePrizeById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let prizeId = req.params.prizeId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "Prize ID is invalid!",
      });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
      if (error) {
        res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Delete prize by ID successfully!",
      });
    });
  };
  module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    uppdatePrizeById,
    deletePrizeById,
  };