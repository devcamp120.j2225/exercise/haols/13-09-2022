// Import thư viện mongoose
const mongoose = require("mongoose");

// Import user model
const voucherModel = require("../models/voucherModel");

// Create
const createVoucher = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let body = req.body;
    console.log(body);
    // B2: Validate dữ liệu
    if (!body.code) {
      res.status(400).json({
        message: "code is required!",
      });
    }
    if(!Number.isInteger(body.discount) || body.discount > 100 || body.discount < 0){
        res.status(400).json({
            message: "Discount is required(0 to 100 '%')!",
          });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let newVoucher = {
      _id: mongoose.Types.ObjectId(),
      code: body.code,
      discount: body.discount,
      note: body.note
    };
    voucherModel.create(newVoucher, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(201).json({
        message: "Create a new Voucher successfully",
        newVoucher: data,
      });
    });
  };

  // Get all 
const getAllVoucher = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    // B2: Validate dữ liệu
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.find((error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Get all Voucher successfully",
        vouchers: data,
      });
    });
  };

  // Get by Id
const getVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let VoucherId = req.params.voucherId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(VoucherId)) {
      return res.status(400).json({
        message: "Voucher ID is invalid!",
      });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findById(VoucherId, (error, data) => {
      if (error) {
        res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Get user by ID successfully!",
        Voucher: data,
      });
    });
  };

  // Update user by Id
const uppdateVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let VoucherId = req.params.voucherId;
    let body = req.body;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(VoucherId)) {
      return res.status(400).json({
        message: "Voucher ID is invalid!",
      });
    }
    if (body.code !== undefined && body.code == "") {
      res.status(400).json({
        message: "code is required!",
      });
    }
    if(!Number.isInteger(body.discount) || body.discount > 100 || body.discount < 0){
        res.status(400).json({
            message: "Discount is required(0 to 100 '%')!",
          });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    let updateVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note
    };
    voucherModel.findByIdAndUpdate(VoucherId, updateVoucher, (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(201).json({
        message: "Update Voucher successfully",
        updateVoucher: data,
      });
    });
  };
  // Delete user by id
const deleteVoucherById = (req, res) => {
    // B1: Thu thập dữ liệu từ req
    let VoucherId = req.params.voucherId;
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(VoucherId)) {
      return res.status(400).json({
        message: "Voucher ID is invalid!",
      });
    }
    // B3: Gọi model thực hiện các thao tác nghiệp vụ
    voucherModel.findByIdAndDelete(VoucherId, (error, data) => {
      if (error) {
        res.status(500).json({
          message: error.message,
        });
      }
      return res.status(200).json({
        message: "Delete Voucher by ID successfully!",
      });
    });
  };
  module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    uppdateVoucherById,
    deleteVoucherById,
  };