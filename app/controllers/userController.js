// Import thư viện mongoose
const mongoose = require("mongoose");

// Import user model
const userModel = require("../models/userModel");

// Create an user
const createUser = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let body = req.body;
  console.log(body);
  // B2: Validate dữ liệu
  if (!body.username) {
    res.status(400).json({
      message: "Username is required!",
    });
  }
  if (!body.firstname) {
    res.status(400).json({
      message: "Firstname is required!",
    });
  }
  if (!body.lastname) {
    res.status(400).json({
      message: "Lastname is required!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newUser = {
    _id: mongoose.Types.ObjectId(),
    username: body.username,
    firstname: body.firstname,
    lastname: body.lastname,
  };
  userModel.create(newUser, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
    return res.status(201).json({
      message: "Create a new user successfully",
      newUser: data,
    });
  });
};

// Get all users
const getAllUsers = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  userModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get all users successfully",
      users: data,
    });
  });
};
// Get user by Id
const getUserById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let userId = req.params.userId;
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID is invalid!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  userModel.findById(userId, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get user by ID successfully!",
      user: data,
    });
  });
};
// Update user by Id
const uppdateUserById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let userId = req.params.userId;
  let body = req.body;
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID is invalid!",
    });
  }
  if (body.username !== undefined && body.username == "") {
    res.status(400).json({
      message: "Username is required!",
    });
  }
  if (body.firstname !== undefined && body.firstname == "") {
    res.status(400).json({
      message: "Firstname is required!",
    });
  }
  if (body.lastname !== undefined && body.lastname == "") {
    res.status(400).json({
      message: "Lastname is required!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let updateUser = {
    username: body.username,
    firstname: body.firstname,
    lastname: body.lastname,
  };
  userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
    return res.status(201).json({
      message: "Update user successfully",
      updateUser: data,
    });
  });
};
// Delete user by id
const deleteUserById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let userId = req.params.userId;
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return res.status(400).json({
      message: "User ID is invalid!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  userModel.findByIdAndDelete(userId, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Delete course by ID successfully!",
    });
  });
};

module.exports = {
  createUser,
  getAllUsers,
  getUserById,
  uppdateUserById,
  deleteUserById,
};
