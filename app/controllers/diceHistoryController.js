const mongoose = require("mongoose");

// Import user model
const diceHistoryModel = require("../models/diceHistoryModel");

// Create an user
const createDiceHistory = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let body = req.body;
  console.log(body);
  // B2: Validate dữ liệu
  if (!Number.isInteger(body.dice) || body.dice > 6 || body.dice < 1) {
    res.status(400).json({
      message: "Dice is required (1 to 6)!",
    });
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newDiceHistory = {
    _id: mongoose.Types.ObjectId(),

    dice: body.dice,
  };
  diceHistoryModel.create(newDiceHistory, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
    return res.status(201).json({
      message: "Create a new dice history successfully",
      newDiceHistory: data,
    });
  });
};
//get all dice history
const getAllDiceHistory = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  diceHistoryModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get all dice history successfully",
      DiceHistory: data,
    });
  });
};
// Get dice history by Id
const getDiceHistoryById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let diceHistoryById = req.params.diceHistoryId;
  console.log(diceHistoryById);
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryById)) {
    return res.status(400).json({
      message: "Dice history ID is invalid!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  diceHistoryModel.findById(diceHistoryById, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Get dice history by ID successfully!",
      diceHistory: data,
    });
  });
};
// Update dice history by Id
const updateDiceHistoryById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let diceHistoryId = req.params.diceHistoryId;
  let body = req.body;
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "User ID is invalid!",
    });
  }
  if (
    !Number.isInteger(body.dice) ||
    body.dice == "" ||
    body.dice > 6 ||
    body.dice < 1
  ) {
    res.status(400).json({
      message: "Dice is required (1 to 6)!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let updateDiceHistory = {
    dice: body.dice,
  };
  diceHistoryModel.findByIdAndUpdate(
    diceHistoryId,
    updateDiceHistory,
    (error, data) => {
      if (error) {
        return res.status(500).json({
          message: error.message,
        });
      }
      return res.status(201).json({
        message: "Update user successfully",
        updateDiceHistory: data,
      });
    }
  );
};
// Delete by id
const deleteDiceHistoryById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let diceHistoryId = req.params.diceHistoryId;
  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
    return res.status(400).json({
      message: "dice History ID is invalid!",
    });
  }
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  diceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
    if (error) {
      res.status(500).json({
        message: error.message,
      });
    }
    return res.status(200).json({
      message: "Delete dice history by ID successfully!",
    });
  });
};
module.exports = {
  createDiceHistory,
  getAllDiceHistory,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById,
};
